#### Install python3

##### Example for debian/ubuntu
`apt-get update && apt-get install python3`


#### Run tests
 
###### Next command  must be run in directory containing package `calc`
`python3 -m unittest`


#### Evaluate

##### Syntax
`./scalc [ OPEARATOR OPERAND SET ]`
 	
	Expression: starts from `[` and ends with `]`
	Operator: 	
		LE: less (preserve only values, that are less than OPERANDs value)
		GR: greater (preserve only values, that are greater than OPERANDs value)
		EQ: equal (preserve only values, that are equal to OPERANDs value)
	Operand: integer value >= 1
	Set: File or Expression, returning unique values
	File: path to file with unique value (one value per row)

##### Files for test evaluation can be found in directory `files`
```
# a.txt
1
2
3

# b.txt
2
3
4

# c.txt
1
2
3
4
5
```

`./scalc [ GR 1 files/c.txt [ EQ 3 files/a.txt files/a.txt files/b.txt ] ]`
```
2
3
```

`./scalc [ LE 2 files/a.txt [ GR 1 files/b.txt files/c.txt ] ]`

```
1
4
```

#### Notes

##### `scalc` is a text file (script), so it can be opened for review with any text editor
