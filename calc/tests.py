import contextlib
import os
import unittest
from tempfile import NamedTemporaryFile, TemporaryDirectory
from typing import List

from calc.expression import parse_expression, Expression
from calc.file import File
from calc.operator import (
    create_operator,
    EqualsOperator,
    GreaterOperator,
    LessOperator,
)


class TestOperator(unittest.TestCase):
    test_criterion = 42

    def test_factory(self):
        pairs = (
            ("EQ", EqualsOperator),
            ("GR", GreaterOperator),
            ("LE", LessOperator),
        )

        for op_type, cls in pairs:
            operator = create_operator(op_type, self.test_criterion)
            self.assertIsInstance(operator, cls)
            self.assertEqual(operator._criterion, self.test_criterion)

        self.assertRaises(TypeError, create_operator, op_type='eq', criterion=self.test_criterion)

    def test_equals(self):
        operator = EqualsOperator(self.test_criterion)

        self.assertFalse(operator.filter(self.test_criterion - 1))
        self.assertTrue(operator.filter(self.test_criterion))
        self.assertFalse(operator.filter(self.test_criterion + 1))

    def test_greater(self):
        operator = GreaterOperator(self.test_criterion)

        self.assertFalse(operator.filter(self.test_criterion - 1))
        self.assertFalse(operator.filter(self.test_criterion))
        self.assertTrue(operator.filter(self.test_criterion + 1))

    def test_less(self):
        operator = LessOperator(self.test_criterion)

        self.assertTrue(operator.filter(self.test_criterion - 1))
        self.assertFalse(operator.filter(self.test_criterion))
        self.assertFalse(operator.filter(self.test_criterion + 1))


class TestFile(unittest.TestCase):
    def test_invalid_path(self):
        with NamedTemporaryFile() as tmp:
            File(tmp.name)

        self.assertRaises(FileNotFoundError, File, tmp.name)

        with TemporaryDirectory() as tmp_dir:
            self.assertRaises(IsADirectoryError, File, tmp_dir)

    def test_get_values(self):
        values = list(range(15))
        with NamedTemporaryFile() as tmp:
            write_values(values, tmp)
            f = File(tmp.name)
            for actual, expected in zip(f.get_values(), values):
                self.assertEqual(actual, expected)


class TestExpression(unittest.TestCase):
    def test_invalid_expression(self):
        raw_expr = "[ LE 2 [ GR 1 ] ]"
        parse_expression(raw_expr.split())

        self.assertRaises(RuntimeError, parse_expression, raw_expr[:-1].split())
        self.assertRaises(RuntimeError, parse_expression, raw_expr[1:].split())

        parse_expression("[ LE 1 ]".split())

        self.assertRaises(ValueError, parse_expression, "[ LE t ]".split())
        self.assertRaises(RuntimeError, parse_expression, "[ LE   ".split())

    def test_files_only(self):
        with create_test_files() as (a, b, c):
            raw_expr = "[ LE 2 {file} ]".format(file=c.name)

            expr = parse_expression(raw_expr.split())
            self.assertEqual(len(expr._subsets), 1)

            actual = list(expr.get_values())
            expected = read_values(c)
            self.assertEqual(actual, expected)

            raw_expr = "[ GR 0 {a} {b} {c} ]".format(
                a=a.name,
                b=b.name,
                c=c.name,
            )

            expr = parse_expression(raw_expr.split())
            self.assertEqual(len(expr._subsets), 3)

            actual = set(expr.get_values())
            expected = set(read_values(a)) | set(read_values(b)) | set(read_values(c))
            self.assertEqual(actual, expected)

    def test_blank_set(self):

        def validate_is_blank(expr_to_parse: str):
            expr = parse_expression(expr_to_parse.split())
            actual = list(expr.get_values())
            self.assertEqual(len(actual), 0)

        validate_is_blank("[ LE 2 [ GR 1 ] ]")

        with create_test_files() as (a, b, c):
            validate_is_blank("[ GR 1 {a} ]".format(a=a.name))
            validate_is_blank("[ EQ 0 {a} ]".format(a=a.name))

            raw_expr = "[ LE 2 {a} {b} {c} [ EQ 1 {b} {c} ] ]".format(
                a=a.name,
                b=b.name,
                c=c.name,
            )
            validate_is_blank(raw_expr)

    def test_results_are_all_rows(self):

        def validate_contains_all_rows(expr_to_parse: str, *files: NamedTemporaryFile):
            expr = parse_expression(expr_to_parse.split())
            actual = list(sorted(expr.get_values()))

            rows = set()
            for f in files:
                rows |= set(read_values(f))

            expected = list(sorted(rows))

            self.assertEqual(actual, expected)

        with create_test_files() as (a, b, c):
            validate_contains_all_rows("[ EQ 1 {a} ]".format(a=a.name), a)
            raw_expr = "[ GR 0 {a} {b} {c} ]".format(
                a=a.name,
                b=b.name,
                c=c.name,
            )
            validate_contains_all_rows(raw_expr, a, b, c)

            raw_expr = "[ LE 3 {b} {c} ]".format(
                b=b.name,
                c=c.name,
            )
            validate_contains_all_rows(raw_expr, b, c)

    def test_multiple_expressions(self):
        with create_test_files() as (a, b, c):
            raw_expr = "[ GR 1 [ EQ 1 {b} ] [ LE 2 {a} {c} ] ]".format(
                a=a.name,
                b=b.name,
                c=c.name,
            )

            expr = parse_expression(raw_expr.split())
            self.assertEqual(len(expr._subsets), 2)
            self.assertIsInstance(expr._subsets[0], Expression)
            self.assertIsInstance(expr._subsets[1], Expression)

            actual = list(expr.get_values())
            self.assertEqual(actual, [4])

    def test_nested_expressions(self):
        with create_test_files() as (a, b, c):
            # e1 [ EQ 1 {c} {b} ] => 1 , 5
            # e2 [ LE 2 {c} {e1} ] => 2, 3, 4
            # e3 [ GR 1 {a} {e2} ] => 2, 3
            # e4 [ LE 3 {e3} {b}, {c} ] => 1, 4, 5
            # e5 [ LE 2 {a} {c} ] => 4, 5
            # e5 [ GR 2 {e4} {a} {e5} ] => 1, 4, 5
            raw_expr = (
                "[ GR 1 " 
                "[ LE 3 [ GR 1 {a} [ LE 2 {c} [ EQ 1 {c} {b} ] ] ] {b} {c} ] "  # e4
                "{a} "
                "[ LE 2 {a} {c} ] "  # e5
                " ]"
            ).format(
                a=a.name,
                b=b.name,
                c=c.name,
            )

            expr = parse_expression(raw_expr.split())

            self.assertEqual(len(expr._subsets), 3)
            self.assertIsInstance(expr._subsets[0], Expression)
            self.assertIsInstance(expr._subsets[1], File)
            self.assertIsInstance(expr._subsets[2], Expression)

            actual = list(expr.get_values())
            self.assertEqual(actual, [1, 4, 5])

    def test_random_expressions(self):
        blank_expr = "[ LE 2 [ GR 1 ] ]"
        expr = parse_expression(blank_expr.split())
        values = list(expr.get_values())
        self.assertEqual(len(values), 0)

        with create_test_files() as (a, b, c):
            base_expr = "[ GR 1 {c} [ EQ 3 {a} {a} {b} ] ]".format(
                a=a.name,
                b=b.name,
                c=c.name,
            )
            expr = parse_expression(base_expr.split())

            values = list(expr.get_values())
            self.assertEqual(values, [2, 3])

            base_expr = "[ LE 2 {a} [ GR 1 {b} {c} ] ]".format(
                a=a.name,
                b=b.name,
                c=c.name,
            )
            expr = parse_expression(base_expr.split())

            values = list(expr.get_values())
            self.assertEqual(values, [1, 4])


@contextlib.contextmanager
def create_test_files():
    with NamedTemporaryFile() as a, NamedTemporaryFile() as b, NamedTemporaryFile() as c:
        write_values([1, 2, 3], a)
        write_values([2, 3, 4], b)
        write_values([1, 2, 3, 4, 5], c)

        yield a, b, c


def write_values(values: List[int], file: NamedTemporaryFile):
    for val in values:
        line = '{}{}'.format(val, os.linesep)
        file.write(line.encode())

    file.seek(0)


def read_values(file: NamedTemporaryFile) -> List[int]:
    values = [int(val) for val in file.readlines()]
    file.seek(0)
    return values
