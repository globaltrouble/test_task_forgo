import abc


def create_operator(op_type: str, criterion: int) -> "Operator":
    """Factory to create operator by it's type, with bound to criterion"""
    cls = {
        "EQ": EqualsOperator,
        "GR": GreaterOperator,
        "LE": LessOperator,
    }.get(op_type)
    if cls is None:
        raise TypeError("Unknown operator: {}".format(op_type))

    return cls(criterion)


class Operator:
    """Operator used to filter some value according to the criteria and operator logic"""

    def __init__(self, criterion: int):
        self._criterion = criterion

    @abc.abstractmethod
    def filter(self, val: int) -> bool:
        pass


class EqualsOperator(Operator):
    def filter(self, val: int) -> bool:
        return val == self._criterion


class GreaterOperator(Operator):
    def filter(self, val: int) -> bool:
        return val > self._criterion


class LessOperator(Operator):
    def filter(self, val: int) -> bool:
        return val < self._criterion
