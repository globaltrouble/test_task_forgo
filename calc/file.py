import os
from typing import Iterator

from calc.set import ISet


class File(ISet):
    """Read rows from a target file and transforms to a set of integers"""

    def __init__(self, file_path: str):
        super(File, self).__init__()
        self._file_path: str = os.path.abspath(file_path)

        if not os.path.exists(self._file_path):
            raise FileNotFoundError("Path doesn't exist:`{}`".format(self._file_path))

        if os.path.isdir(self._file_path):
            raise IsADirectoryError("Is a directory: `{}`".format(self._file_path))

    def get_values(self) -> Iterator[int]:
        """Returns file rows casted to integers"""
        with open(self._file_path, "r") as file:
            for line in file:
                yield int(line)
