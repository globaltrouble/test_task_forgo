import abc
from typing import Iterator


class ISet:
    """Declares common interface for iteration through set of values"""

    @abc.abstractmethod
    def get_values(self) -> Iterator[int]:
        pass
