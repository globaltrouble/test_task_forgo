import heapq
from typing import List, Iterator

from calc.operator import Operator, create_operator
from calc.set import ISet
from calc.file import File


EXPRESSION_BEGIN = '['
EXPRESSION_END = ']'


def parse_expression(args: List[str]) -> "Expression":
    """Transforms list of string arguments to a single expression"""
    args = list(reversed(args))
    expressions: List[Expression] = []

    # According to test task there is no need to handle particular validation errors
    # so do not create special type for different validation errors.
    # Common RuntimeError exception class is used for all kinds of errors.
    if not args[-1] == EXPRESSION_BEGIN:
        raise RuntimeError(
            "Expression must starts with leading, `[`, wrong expr: `{}`".format(
                ' '.join(args)
            )
        )

    # Two stack algorithm for expression parsing.
    while args:
        arg = args.pop()

        if arg == EXPRESSION_BEGIN:
            if len(args) < 2:
                raise RuntimeError("Too short expression: `{}`".format(' '.join(args)))

            operator = create_operator(op_type=args.pop(), criterion=int(args.pop()))
            expressions.append(Expression(operator))

        elif arg == EXPRESSION_END:
            expr = expressions.pop()

            # Base case, to stop parsing.
            if not expressions:
                if args:
                    raise RuntimeError("Unused params in expression! `{}`".format(' '.join(args)))

                return expr

            expressions[-1].add_set(expr)

        else:
            expressions[-1].add_set(File(arg))

    raise RuntimeError("Incomplete expression! {} expressions were not closed!".format(len(expressions)))


class Expression(ISet):
    """Expression transforms array of sets to a single set with filtered by operator"""

    def __init__(self, operator: Operator):
        super(Expression, self).__init__()

        self._operator: Operator = operator
        self._subsets: List[ISet] = []

    def add_set(self, subset: ISet):
        """Extends resulting set with values from subset"""

        # Extra optimization can be added, In case there are duplicates in files in the same expression
        # score for each value can be increased by the count of duplicates during the score calculation.
        # Duplicates can be found using realpath to the file (to handle symlinks).
        # Similar optimization can be added for nested_expressions

        self._subsets.append(subset)

    def get_values(self) -> Iterator[int]:
        sources = {}
        pq = []

        subsets = [s.get_values() for s in self._subsets]
        self._read_next_values(subsets, sources, pq)

        while pq:
            # pop sources for min value, as rows in files are ordered there are no more rows with the same value,
            # because each next row will be greater than previous.
            # And so we know the exact number of occurrences for min value.
            val = heapq.heappop(pq)
            subsets = sources.pop(val)
            if self._operator.filter(len(subsets)):
                yield val

            self._read_next_values(subsets, sources, pq)

        sources.clear()
        pq.clear()

    @classmethod
    def _read_next_values(cls, subsets, sources, pq):
        for subset in subsets:
            val = cls._get_next_value(subset)
            if val is None:
                continue

            if val in sources:
                sources[val].append(subset)
            else:
                heapq.heappush(pq, val)
                sources[val] = [subset]

    @staticmethod
    def _get_next_value(subset):
        try:
            return next(subset)
        except StopIteration:
            return None
